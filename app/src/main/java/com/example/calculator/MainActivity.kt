package com.example.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.math.sqrt

class MainActivity : AppCompatActivity() {

    lateinit var txtInput: TextView

    private var lastNumeric: Boolean = false

    private var lastDot: Boolean = false

    var oldNum = ""
    var operation = "+"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        txtInput = findViewById(R.id.txtInput)
    }

    fun onDigit(view: View) {
        if (txtInput.text.startsWith("0")){
            this.txtInput.text = ""
            txtInput.append((view as Button).text)
        } else{
            txtInput.append((view as Button).text)
            lastNumeric = true
        }
    }

    fun onDecimalPoint(view: View) {
        if (lastNumeric && !lastDot) {
            txtInput.append(".")
            lastNumeric = false
            lastDot = true
        }
    }

    fun onOperator(view: View) {
        oldNum = txtInput.text.toString()
        val ids = view as Button
        when(ids.id){
            plus_btn.id -> {operation = "+"}
            minus_btn.id -> {operation = "-"}
            multiply_btn.id -> {operation = "*"}
            divide_btn.id -> {operation = "/"}
            percent_btn.id -> {operation = "%"}
        }
        txtInput.append(operation)
        lastNumeric = false
        lastDot = false
    }

    fun onClear(view: View) {
        this.txtInput.text = ""
        lastNumeric = false
        lastDot = false
    }

    fun onChange(view: View){
        val opposite = this.txtInput.text.toString().toInt() * -1
        this.txtInput.text = ""
        txtInput.append(opposite.toString())
        lastNumeric = false
    }

    fun onSqrRoot(view: View){
        val root = sqrt(txtInput.text.toString().toDouble())
        this.txtInput.text = ""
        txtInput.append(root.toString())
    }

    fun onEqual(view: View){
        val newNum = txtInput.text.toString()
        var result = 0
        when(operation){
            "+" -> {result = oldNum.toInt() + newNum.split("+")[1].toInt()}
            "-" -> {result = oldNum.toInt() - newNum.split("-")[1].toInt()}
            "*" -> {result = oldNum.toInt() * newNum.split("*")[1].toInt()}
            "/" -> {result = oldNum.toInt() / newNum.split("/")[1].toInt()}
            "%" -> {result = oldNum.toInt() * newNum.split("%")[1].toInt() / 100}
        }
        this.txtInput.text = ""
        txtInput.append(result.toString())
    }
}